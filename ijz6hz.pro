bias1 = readfits('bias1.fits', headerb1) 
bias2 = readfits('bias2.fits', headerb2)
flat1 = readfits('flat1.fits', headerf1)
flat2 = readfits('flat2.fits', headerf2)

bias1 = bias1 * 1.0
bias2 = bias2 * 1.0
flat1 = flat1 * 1.0
flat2 = flat2 * 1.0


print, "bias x ", fxpar(headerb1, 'NAXIS1')
print, "bias y ", fxpar(headerb1, 'NAXIS2')

;SIGMA NÉLKÜL

meanb1 = mean(bias1)
meanb2 = mean(bias2)
meanf1 = mean(flat1)
meanf2 = mean(flat2)
print, "mean b1= ",  meanb1
print, "mean b2= ", meanb2
print, "mean f1= ", meanf1
print, "mean f2= ", meanf2

varb = variance(bias1 - bias2)
varf = variance(flat1 - flat2) 
print, "var bias= ", varb
print, "var flat= ", varf

devb = stddev(bias1 - bias2)
print, "std bias= ", devb

G1 = ((meanf1 + meanf2) - (meanb1 + meanb2)) / (varf - varb)
print, "Gain (nosigma) = ", G1
RN1 = (G1 * devb) / sqrt(2) 
print, "Reading noise (nosigma) = ", RN1

output = 'bias.ps'	;kimenet
xs = 20			;xsize
ys = 8			;ysize
xtit = 'Intezitas'		;x title
ytit = 'Darabszam'		;y title
ptit = 'Bias hisztogram'		;title
c1 = 'ff9933'x 		;első szín
c2 = ''x 		;második szín
c3 = ''x 		;harmadik szín (ha kell)
ptype = 'ps' 		;plottype

orig_device=!d.name
set_plot, ptype
device, decomposed=1, /color
device, filename=output, xsize=xs, ysize=ys
device, /ISOLATIN1
plot, histogram(bias1), xtitle = xtit, xra = [0, 100], $
ytitle = ytit, title = ptit, xstyle = 1
oplot, histogram(bias1), color = c1
device, /close_file
set_plot, orig_device

output = 'flat.ps'	;kimenet
xs = 20			;xsize
ys = 8			;ysize
xtit = 'Intezitas'		;x title
ytit = 'Darabszam'		;y title
ptit = 'Flat hisztogram'		;title
c1 = 'ff9933'x 		;első szín
c2 = ''x 		;második szín
c3 = ''x 		;harmadik szín (ha kell)
ptype = 'ps' 		;plottype

orig_device=!d.name
set_plot, ptype
device, decomposed=1, /color
device, filename=output, xsize=xs, ysize=ys
device, /ISOLATIN1
plot, histogram(flat1), xtitle = xtit, xra = [0, 120], $
ytitle = ytit, title = ptit, xstyle = 1
oplot, histogram(flat1), color = c1
device, /close_file
set_plot, orig_device

;SIGMÁVAL

sigmab1 = stddev(bias1)
sigmab2 = stddev(bias2)
sigmaf1 = stddev(flat1)
sigmaf2 = stddev(flat2)

print, 'sgb 1= ', sigmab1
print, 'sgb 2= ', sigmab2
print, 'sgf 1= ', sigmaf1
print, 'sgf 2= ', sigmaf2

bias1n = bias1(where(bias1 lt (meanb1 + (3*sigmab1)) and bias1 gt (meanb1 - (3*sigmab1))))
bias2n = bias2(where(bias1 lt (meanb1 + (3*sigmab1)) and bias1 gt (meanb1 - (3*sigmab1)))) 

flat1n = flat1(where(flat1 lt (meanf1 + (3*sigmaf1)) and flat1 gt (meanf1 - (3*sigmaf1))))
flat2n = flat2(where(flat1 lt (meanf1 + (3*sigmaf1)) and flat1 gt (meanf1 - (3*sigmaf1)))))

bias1n = bias1n * 1.0
bias2n = bias2n * 1.0
flat1n = flat1n * 1.0
flat2n = flat2n * 1.0

meanb1s = mean(bias1n)
meanb2s = mean(bias2n)
meanf1s = mean(flat1n)
meanf2s = mean(flat2n)
print, "mean b1s= ",  meanb1s
print, "mean b2s= ", meanb2s
print, "mean f1s= ", meanf1s
print, "mean f2s= ", meanf2s

varbs = variance(bias1n - bias2n)
varfs = variance(flat1n - flat2n) 

print, "var biasS= ", varbs
print, "var flats= ", varfs

devbs = stddev(bias1n - bias2n)
print, "std biasS= ", devbs

G1s = ((meanf1s + meanf2s) - (meanb1s + meanb2s)) / (varfs - varbs)
print, "Gain (sigma) = ", G1s
RN1s = (G1s * devbs) / sqrt(2) 
print, "Reading noise (sigma) = ", RN1s

output = 'biassig.ps'	;kimenet
xs = 20			;xsize
ys = 8			;ysize
xtit = 'Intezitas'		;x title
ytit = 'Darabszam'		;y title
ptit = 'Bias hisztogram'		;title
c1 = 'ff9933'x 		;első szín
c2 = ''x 		;második szín
c3 = ''x 		;harmadik szín (ha kell)
ptype = 'ps' 		;plottype

orig_device=!d.name
set_plot, ptype
device, decomposed=1, /color
device, filename=output, xsize=xs, ysize=ys
device, /ISOLATIN1
plot, histogram(bias1n), xtitle = xtit, xra = [0, 100], $
ytitle = ytit, title = ptit, xstyle = 1
oplot, histogram(bias1n), color = c1
device, /close_file
set_plot, orig_device

output = 'biassiglog.ps'	;kimenet
xs = 20			;xsize
ys = 8			;ysize
xtit = 'Intezitas (log)'		;x title
ytit = 'Darabszam (log)'		;y title
ptit = 'Bias hisztogram'		;title
c1 = 'ff9933'x 		;első szín
c2 = ''x 		;második szín
c3 = ''x 		;harmadik szín (ha kell)
ptype = 'ps' 		;plottype

orig_device=!d.name
set_plot, ptype
device, decomposed=1, /color
device, filename=output, xsize=xs, ysize=ys
device, /ISOLATIN1
plot, histogram(bias1n), xtitle = xtit, xra = [1, 1e3], yra = [1,1.5e6], $
ytitle = ytit, /ylog, /xlog, title = ptit, ystyle = 1, xstyle = 1
oplot, histogram(bias1n), color = c1
device, /close_file
set_plot, orig_device

output = 'flatsig.ps'	;kimenet
xs = 20			;xsize
ys = 8			;ysize
xtit = 'Intezitas'		;x title
ytit = 'Darabszam'		;y title
ptit = 'Flat hisztogram'		;title
c1 = 'ff9933'x 		;első szín
c2 = ''x 		;második szín
c3 = ''x 		;harmadik szín (ha kell)
ptype = 'ps' 		;plottype

orig_device=!d.name
set_plot, ptype
device, decomposed=1, /color
device, filename=output, xsize=xs, ysize=ys
device, /ISOLATIN1
plot, histogram(flat1n), xtitle = xtit, xra = [0, 120], $
ytitle = ytit, title = ptit, xstyle = 1
oplot, histogram(flat1n), color = c1
device, /close_file
set_plot, orig_device

;MÁSIK SIGMA

sigmab1 = stddev(bias1)
sigmab2 = stddev(bias2)
sigmaf1 = stddev(flat1)
sigmaf2 = stddev(flat2)

print, 'sgb 1= ', sigmab1
print, 'sgb 2= ', sigmab2
print, 'sgf 1= ', sigmaf1
print, 'sgf 2= ', sigmaf2

bias1n = bias1(where(bias1 lt (meanb1 + (3*sigmab1)) and bias1 gt (meanb1 - (3*sigmab1))) and (where(bias2 lt (meanb2 + (3*sigmab2)) and bias2 gt (meanb2 - (3*sigmab2)))))
bias2n = bias1(where(bias1 lt (meanb1 + (3*sigmab1)) and bias1 gt (meanb1 - (3*sigmab1))) and (where(bias2 lt (meanb2 + (3*sigmab2)) and bias2 gt (meanb2 - (3*sigmab2)))))

flat1n = flat1(where(flat1 lt (meanf1 + (3*sigmaf1)) and flat1 gt (meanf1 - (3*sigmaf1))) and (where(flat2 lt (meanf2 + (3*sigmaf2)) and flat2 gt (meanf2 - (3*sigmaf2)))))
flat2n = flat1(where(flat1 lt (meanf1 + (3*sigmaf1)) and flat1 gt (meanf1 - (3*sigmaf1))) and (where(flat2 lt (meanf2 + (3*sigmaf2)) and flat2 gt (meanf2 - (3*sigmaf2)))))

bias1n = bias1n * 1.0
bias2n = bias2n * 1.0
flat1n = flat1n * 1.0
flat2n = flat2n * 1.0

meanb1s = mean(bias1n)
meanb2s = mean(bias2n)
meanf1s = mean(flat1n)
meanf2s = mean(flat2n)
print, "mean b1s= ",  meanb1s
print, "mean b2s= ", meanb2s
print, "mean f1s= ", meanf1s
print, "mean f2s= ", meanf2s

varbs = variance(bias1n - bias2n)
varfs = variance(flat1n - flat2n) 

print, "var biasS= ", varbs
print, "var flats= ", varfs

devbs = stddev(bias1n - bias2n)
print, "std biasS= ", devbs

G1s = ((meanf1s + meanf2s) - (meanb1s + meanb2s)) / (varfs - varbs)
print, "Gain (sigma) = ", G1s
RN1s = (G1s * devbs) / sqrt(2) 
print, "Reading noise (sigma) = ", RN1s

output = 'biassig1log.ps'	;kimenet
xs = 20			;xsize
ys = 8			;ysize
xtit = 'Intezitas (log)'		;x title
ytit = 'Darabszam (log)'		;y title
ptit = 'Bias hisztogram'		;title
c1 = 'ff9933'x 		;első szín
c2 = ''x 		;második szín
c3 = ''x 		;harmadik szín (ha kell)
ptype = 'ps' 		;plottype

orig_device=!d.name
set_plot, ptype
device, decomposed=1, /color
device, filename=output, xsize=xs, ysize=ys
device, /ISOLATIN1
plot, histogram(bias1n), xtitle = xtit, /xlog, /ylog, $
ytitle = ytit, xrange = [1,1e3], yrange = [1,1.5e6], title = ptit, xstyle = 1, ystyle = 1
oplot, histogram(bias1n), color = c1
device, /close_file
set_plot, orig_device

output = 'flatsig1.ps'	;kimenet
xs = 20			;xsize
ys = 8			;ysize
xtit = 'Intezitas'		;x title
ytit = 'Darabszam'		;y title
ptit = 'Flat hisztogram'		;title
c1 = 'ff9933'x 		;első szín
c2 = ''x 		;második szín
c3 = ''x 		;harmadik szín (ha kell)
ptype = 'ps' 		;plottype

orig_device=!d.name
set_plot, ptype
device, decomposed=1, /color
device, filename=output, xsize=xs, ysize=ys
device, /ISOLATIN1
plot, histogram(flat1n), xtitle = xtit, xra = [0, 120], yra = [0,2000], $
ytitle = ytit, title = ptit, xstyle = 1, ystyle = 1
oplot, histogram(flat1n), color = c1
device, /close_file
set_plot, orig_device

end
