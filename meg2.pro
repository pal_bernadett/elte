file = 'USNO_biaskorr.fits'
map = readfits(file, header, /silent)
xax = fxpar(header, 'NAXIS1')
yax = fxpar(header, 'NAXIS2')

;window, 0
;tvscl, map > min(map/10000) < 2*mean(map)

; csillag helye: 2182, 2025

cutx = 601
cuty = 401
starx = 2182
stary = 2025

mapcut = map[starx - cutx/2 : starx + cutx/2, stary - cuty/2 : stary + cuty/2]

help, mapcut

;window, 1
;tvscl, mapcut > min(mapcut/10000) < 2*mean(mapcut)

;tvbox, 20, (cutx/2)-1, cuty/2

help, mapcut
print, (cutx/2)-1, cuty/2

; I -> sorok, van 401 db sor 
; J -> oszlopok, van 601 db oszlop
; tehát, minden sornak van 601 db eleme, és minden oszlopnak 401 db 
; csillag: 299 200
; négyzet: 289 - 301, 190 - 210

sum = 0
k = 0

ibegin = 289
iend = 309
jbegin = 190
jend = 210

sorsize = (iend - ibegin) + 1
oszsize = (jend - jbegin) + 1

sorsum = fltarr(sorsize)
oszsum = fltarr(oszsize)

for i = ibegin, iend do begin
	for j = jbegin, jend do begin
		sum += mapcut[i,j]
	endfor
	;print, sum, i
	sorsum[k] = sum
	sum = 0
	k = k + 1
endfor

print, sorsum

print, "másik"

k = 0

for i = jbegin, jend do begin
	for j = ibegin, iend do begin
		sum += mapcut[j,i]
	endfor
	;print, sum, i
	oszsum[k] = sum
	sum = 0
	k = k + 1
endfor

print, oszsum

; 2. lépés

sumi = 0
sumj = 0

;sumi = total(sorsum)
;sumj = total(oszsum)
iend2 = sorsize - 1
jend2 = oszsize - 1

for i = 0, iend2 do begin
	sumi += sorsum[i]
endfor

print, "sumi = ", sumi

ivonas = 1.0/sorsize * sumi
print, "ivonas = ", ivonas

for i = 0, iend2 do begin
	sumj += oszsum[i]
endfor

print, "sumj = ", sumj

jvonas = 1.0/oszsize * sumj
print, "jvonas = ", jvonas

;window, 2
;plot, mapcut[289:301, 200]

;window, 3
;plot, mapcut[295, 190:210]

; 3. lépés -> fontos, csak a 0-nál nagyobb 
; xi -> i változó, yj -> j változó

starcentx = 0
starcenty = 0

tempup = 0 ;tört számláló
tempdown = 0 ;tört nevező

for i = 0, iend2 do begin
	if (sorsum[i] - ivonas) > 0 then begin
		tempup += (sorsum[i] - ivonas) * (i+ibegin)
		tempdown += sorsum[i] - ivonas
	endif	
endfor

print, tempup
print, tempdown
starcentx = tempup / tempdown
print, "xc = ", starcentx

tempup = 0
tempdown = 0

for i = 0, jend2 do begin
	if (oszsum[i] - jvonas) > 0 then begin
		tempup += (oszsum[i] - jvonas) * (i+jbegin)
		tempdown += oszsum[i] - jvonas
	endif		
endfor

print, tempup
print, tempdown
starcenty = tempup / tempdown
print, "yc = ", starcenty

; 4. lépés, GAUSS

; x irányba a centrális mentén ilesztünk egy gausst, meg az y mentén is
; és a két FWHM átlaga lesz az FWHM

plotarr = findgen(sorsize)

;window, 0
;plot, mapcut[starcentx,190:210]

;window, 1
;plot, mapcut[289:309, starcenty]

yfit = gaussfit(plotarr, mapcut[starcentx,jbegin:jend], coeff, nterms = 6)
print, 'Gauss x: ', coeff

fwhmx = 2*sqrt(2*alog(2))*coeff[2] 
print, "FWHM x: ", fwhmx

yfit2 = gaussfit(plotarr, mapcut[ibegin:iend, starcenty], coeff2, nterms = 6)
print, 'Gauss y: ', coeff2

fwhmy = 2*sqrt(2*alog(2))*coeff2[2] 
print, "FWHM y: ", fwhmy

fwhmm = (fwhmx + fwhmy) / 2.0
print, "FWHM átlag: ", fwhmm

; 5. lépés - kör rajzolás

window, 2
tvscl, mapcut > min(mapcut/10000) < 2*mean(mapcut)

;tvbox, 20, (cutx/2), cuty/2
tvcircle, fwhmm*3, starcentx, starcenty, red
tvcircle, fwhmm*6, starcentx, starcenty, blue
tvcircle, fwhmm*9, starcentx, starcenty, blue

; 6. lépés: Apertúra összegzés

istart = (starcentx - (fwhmm*3) - 1)
iend = (starcentx + (fwhmm*3) + 1)
jstart = (starcenty - (fwhmm*3) - 1)
jend = (starcenty + (fwhmm*3) + 1)

;print, "istart= ", istart
;print, "iend= ", iend
;print, "jstart= ", jstart
;print, "jend= ", jend

apsum = 0
pixnum = 0

for i = istart, iend do begin
	;print, "Entered the first for cycle - i\n"
	for j = jstart, jend do begin
		;print, "Entered the second for cycle - j\n"
		if (rr = sqrt(((j - starcenty)^2) + ((i - starcentx)^2)) lt (fwhmm*3)) then begin
			;print, "Entered if statement\n"
			apsum += mapcut[i,j]
			pixnum += 1
			;print, "Apsum= ", apsum
		endif
	endfor
endfor

print, "Apsum= ", apsum
print, "Pixnum= ", pixnum
print, "Average= ", apsum / pixnum

;kell, hogy hány pixelre számoltunk -> 9, 6 szigmás gyűrűk és akkor adom hozzá, ha a kettő között van

; 7. lépés: Háttér

istart = (starcentx - (fwhmm*9) - 1)
iend = (starcentx + (fwhmm*9) + 1)
jstart = (starcenty - (fwhmm*9) - 1)
jend = (starcenty + (fwhmm*9) + 1)

;print, "istart= ", istart
;print, "iend= ", iend
;print, "jstart= ", jstart
;print, "jend= ", jend
print, "-------------------------------------"
print, "Background"
print, "-------------------------------------"

apsum2 = 0
pixnum2 = 0

for i = istart, iend do begin
	;print, "Entered the first for cycle - i\n"
	for j = jstart, jend do begin
		rr = sqrt(((j - starcenty)^2) + ((i - starcentx)^2))
		;print, "Entered the second for cycle - j\n"
		if ((rr gt (fwhmm*6)) and (rr lt (fwhmm*9))) then begin
			;print, "Entered if statement\n"
			apsum2 += mapcut[i,j]
			pixnum2 += 1
			;print, "Apsum= ", apsum
		endif
	endfor
endfor

print, "Apsum2= ", apsum2
print, "Pixnum2= ", pixnum2
print, "Average= ", apsum2 / pixnum2
print, "-------------------------------------"
print, "Value= ", apsum - (pixnum * (apsum2 / pixnum2))
print, "-------------------------------------"
print, "... Rescaling ..."

; 8. lépés: Százalékos területarány számítás

;Nagyítás

ratio = 3.5
ratio2 = ratio*ratio

origx = n_elements(map [*,0])
origy = n_elements(map [0,*])

print, "origx", origx
print, "origy", origy

newx = origx * ratio
newy = origy * ratio

mapcon = congrid(map, newx, newy) 
help, mapcon

;window, 3, retain = 2, xsize = newx, ysize = newy
;tvscl, mapcon > min(mapcon/10000) < 2*mean(mapcon)

;1.

newcutx = cutx * ratio
newcuty = cuty * ratio
newstarx = starx * ratio
newstary = stary * ratio

print, "cutx, cuty ", cutx, cuty
print, "newcutx, newcuty ", newcutx, newcuty
print, "starx, stary ", starx, stary
print, "newstarx, newstary ", newstarx, newstary

newmapcut = mapcon[newstarx - newcutx/2 : newstarx + newcutx/2, newstary - newcuty/2 : newstary + newcuty/2]

help, newmapcut

sum = 0
k = 0

newibegin = floor(ibegin * ratio) 
newiend = floor(iend * ratio) + 1
newjbegin = floor(jbegin * ratio)
newjend = floor(jend * ratio)

newsorsize = (newiend - newibegin) + 1
newoszsize = (newjend - newjbegin) + 1

newsorsum = fltarr(newsorsize)
newoszsum = fltarr(newoszsize)

print, "ibegin, iend ", ibegin, iend
print, "newibegin, newiend ", newibegin, newiend
help, sorsum
help, newsorsum

for i = newibegin, newiend do begin
	for j = newjbegin, newjend do begin
		sum += newmapcut[i,j]
	endfor
	;print, sum, i
	newsorsum[k] = sum
	sum = 0
	k = k + 1
endfor

;print, newsorsum

print, "másik"

k = 0

for i = newjbegin, newjend do begin
	for j = newibegin, newiend do begin
		sum += newmapcut[j,i]
	endfor
	;print, sum, i
	newoszsum[k] = sum
	sum = 0
	k = k + 1
endfor

;print, newoszsum

;2

sumi = 0
sumj = 0

newiend2 = newsorsize - 1
newjend2 = newoszsize - 1

for i = 0, newiend2 do begin
	sumi += newsorsum[i]
endfor

print, "sumi = ", sumi

newivonas = 1.0/newsorsize * sumi
print, "ivonas = ", newivonas

for i = 0, newiend2 do begin
	sumj += newoszsum[i]
endfor

print, "sumj = ", sumj

newjvonas = 1.0/newoszsize * sumj
print, "jvonas = ", newjvonas

;3

newstarcentx = 0
newstarcenty = 0

tempup = 0 ;tört számláló
tempdown = 0 ;tört nevező

for i = 0, newiend2 do begin
	if (newsorsum[i] - newivonas) > 0 then begin
		tempup += (newsorsum[i] - newivonas) * (i+newibegin)
		tempdown += newsorsum[i] - newivonas
	endif	
endfor

print, tempup
print, tempdown
newstarcentx = tempup / tempdown
print, "xc = ", newstarcentx

tempup = 0
tempdown = 0

for i = 0, newjend2 do begin
	if (newoszsum[i] - newjvonas) > 0 then begin
		tempup += (newoszsum[i] - newjvonas) * (i+newjbegin)
		tempdown += newoszsum[i] - newjvonas
	endif		
endfor

print, tempup
print, tempdown
newstarcenty = tempup / tempdown
print, "yc = ", newstarcenty

;4

newplotarr = findgen(newsorsize)

yfit = gaussfit(newplotarr, newmapcut[newstarcentx,newjbegin:newjend], coeff, nterms = 6)
print, 'Gauss x: ', coeff

fwhmx = 2*sqrt(2*alog(2))*coeff[2] 
print, "FWHM x: ", fwhmx

yfit2 = gaussfit(newplotarr, newmapcut[newibegin:newiend, newstarcenty], coeff2, nterms = 6)
print, 'Gauss y: ', coeff2

fwhmy = 2*sqrt(2*alog(2))*coeff2[2] 
print, "FWHM y: ", fwhmy

fwhmm = (fwhmx + fwhmy) / 2.0
print, "FWHM átlag: ", fwhmm

;5

window, 4, retain = ratio, xsize = newx, ysize = newy
tvscl, newmapcut > min(newmapcut/10000) < 2*mean(newmapcut)

;tvbox, 20, (cutx/2), cuty/2
tvcircle, fwhmm*3, newstarcentx, newstarcenty, red
tvcircle, fwhmm*6, newstarcentx, newstarcenty, blue
tvcircle, fwhmm*9, newstarcentx, newstarcenty, blue

;6

newistart = (newstarcentx - (fwhmm*3) - 1)
newiend = (newstarcentx + (fwhmm*3) + 1)
newjstart = (newstarcenty - (fwhmm*3) - 1)
newjend = (newstarcenty + (fwhmm*3) + 1)

;print, "istart= ", istart
;print, "iend= ", iend
;print, "jstart= ", jstart
;print, "jend= ", jend

newapsum = 0
newpixnum = 0

for i = newistart, newiend do begin
	;print, "Entered the first for cycle - i\n"
	for j = newjstart, newjend do begin
		;print, "Entered the second for cycle - j\n"
		if (rr = sqrt(((j - newstarcenty)^2) + ((i - newstarcentx)^2)) lt (fwhmm*3)) then begin
			;print, "Entered if statement\n"
			newapsum += newmapcut[i,j] / ratio2 ;
			newpixnum += 1
			;print, "Apsum= ", apsum
		endif
	endfor
endfor

print, "Apsum= ", newapsum
print, "Pixnum= ", newpixnum
print, "Average= ", newapsum / newpixnum

;7

newistart = (newstarcentx - (fwhmm*9) - 1)
newiend = (newstarcentx + (fwhmm*9) + 1)
newjstart = (newstarcenty - (fwhmm*9) - 1)
newjend = (newstarcenty + (fwhmm*9) + 1)

print, "-------------------------------------"
print, "New Background"
print, "-------------------------------------"

newapsum2 = 0
newpixnum2 = 0

for i = newistart, newiend do begin
	;print, "Entered the first for cycle - i\n"
	for j = newjstart, newjend do begin
		rr = sqrt(((j - newstarcenty)^2) + ((i - newstarcentx)^2))
		;print, "Entered the second for cycle - j\n"
		if ((rr gt (fwhmm*6)) and (rr lt (fwhmm*9))) then begin
			;print, "Entered if statement\n"
			newapsum2 += newmapcut[i,j] / ratio2;
			newpixnum2 += 1
			;print, "Apsum= ", apsum
		endif
	endfor
endfor

print, "Apsum2= ", newapsum2
print, "Pixnum2= ", newpixnum2
print, "Average= ", newapsum2 / newpixnum2
print, "-------------------------------------"
print, "New Value= ", newapsum - (newpixnum * (newapsum2 / newpixnum2))

end
